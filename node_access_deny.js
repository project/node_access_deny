/**
 * @file
 * Provides JavaScript additions to autocomplete widget
 * The origin code of nodeAccessDenyHelperAction function was developed by @marvil for D6 version of og_username_helper module
 *
 */
(function($){

Drupal.behaviors.nodeAccessDeny = {};
Drupal.behaviors.nodeAccessDeny.attach = function(context, settings) {
  if (Drupal.jsAC) {
    Drupal.jsAC.prototype.hidePopup = function (keycode) {
  // Select item if the right key or mousebutton was pressed.
  if (this.selected && ((keycode && keycode != 46 && keycode != 8 && keycode != 27) || !keycode)) {
    this.input.value = $(this.selected).data('autocompleteValue');
  }
  // Hide popup.
  var popup = this.popup;
  if (popup) {
    this.popup = null;
    $(popup).fadeOut('fast', function () { $(popup).remove(); });
    Drupal.nodeAccessDeny.nodeAccessDenyHelperAction();
  }
  this.selected = false;
  $(this.ariaLive).empty();
    };
  }
};

Drupal.nodeAccessDeny = Drupal.nodeAccessDeny || {
  nodeAccessDenyHelperAction: function(){
    $("#edit-node-access-deny-helper").val($.trim($("#edit-node-access-deny-helper").val()));
    if ($("#edit-node-access-deny-helper").val() != undefined && $("#edit-node-access-deny-helper").val() != '') {
      var coma=',';
      if ( $("#edit-mails").val() != undefined) {
        if ( $("#edit-mails").val() == '')
          coma='';
        if ( $.inArray($("#edit-node-access-deny-helper").val(), $("#edit-mails").val().split(",")) < 0)
          $("#edit-mails").val($("#edit-mails").val()+coma+$("#edit-node-access-deny-helper").val());
      }
      else if ( $("#edit-node-access-deny-nids").val() != undefined ) {
        if ($("#edit-node-access-deny-nids").val()=='')
          coma='';
        if ( $.inArray($("#edit-node-access-deny-helper").val(), $("#edit-node-access-deny-nids").val().split(",")) < 0)
          $("#edit-node-access-deny-nids").val($("#edit-node-access-deny-nids").val()+coma+$("#edit-node-access-deny-helper").val());
      }
    }
    $("#edit-node-access-deny-helper").val('');
    return false;
  }
}
})(jQuery);
